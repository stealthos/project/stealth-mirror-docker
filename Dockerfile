FROM caddy:2.7.4-alpine

# Install Needed Packages
RUN apk update

RUN apk add rsync

RUN apk add bash

# Copy Source Files
COPY ./src/start.sh ./start.sh

RUN chmod +x ./start.sh

COPY ./src/rsync.conf /rsync.conf

COPY ./src/Caddyfile /etc/caddy/Caddyfile

COPY ./src/sync-mirror.sh /sync-mirror.sh

# Setup Crontab for every 4 hours
RUN echo "0 */4 * * * /bin/bash /sync-mirror.sh" >> /var/spool/cron/crontabs/root

CMD ./start.sh
