#!/bin/bash

# Define the directory to check
directory="/mirror"

#Ensure Script is executable
chmod +x /sync-mirror.sh

# Check if the directory is empty
if [ -z "$(ls -A $directory 2>/dev/null)" ]; then
    export FIRST="first"
    echo "Running Inital Mirror Sync"
    /sync-mirror.sh
else
    echo "Skipping Inital Mirror Sync since /mirror already has files"
fi

echo "Starting Caddy(Web Server) & Crontab & RSync Daemon"
rsync --daemon --config=/rsync.conf
crond && caddy run --config /etc/caddy/Caddyfile